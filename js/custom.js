	jQuery(document).ready(function ($) {
		  $('.zoom').magnify();
		$('.siteLoader').hide();
		$('.mainSite').css('display','block');
		$('.myTable').DataTable();
		$('#proAddsubcat').prop('disabled', 'disabled');
		$('#proAddCat').on('change',function(){
			var cat_id=$('#proAddCat').val();
			var cat_id_token=$('#proAddsubcatToken').val();
			var url=window.location.protocol + "//" + window.location.host;
			$.ajax({
				type: "POST",
				url: url + '/getSubcatPro/'+cat_id,
				data: {id: cat_id,_token:cat_id_token},
				success: function( msg ) {
					$('#proAddsubcat').prop('disabled', false);
					$("#proAddsubcat").html(msg);
				}
			});
		});
		///Slider
		$('.control_next').fadeOut( "slow" );
		$('.control_prev').fadeOut( "slow" );
		$('#slider').hover(function() {
			$('.control_next').fadeToggle("slow");
			$('.control_prev').fadeToggle("slow");
		});
		
		setInterval(function () {
			moveRight();
		}, 3000);
		var slideCount = $('#slider ul li').length;
		var slideWidth = $('#slider ul li').width();
		var slideHeight = $('#slider ul li').height();
		var sliderUlWidth = slideCount * slideWidth;

		$('#slider').css({ width: slideWidth, height: slideHeight });

		function moveLeft() {
			$('#slider ul').animate({
				left: + slideWidth
			}, 200, function () {
				$('#slider ul li:last-child').prependTo('#slider ul');
				$('#slider ul').css('left', '');
			});
		};

		function moveRight() {
			$('#slider ul').animate({
				left: - slideWidth
			}, 200, function () {
				$('#slider ul li:first-child').appendTo('#slider ul');
				$('#slider ul').css('left', '');
			});
		};

		$('a.control_prev').click(function () {
			moveLeft();
		});

		$('a.control_next').click(function () {
			moveRight();
		});
		//SLIDER END
		$('.catpanel').fadeOut();
		$('.catpanel ul li').find('ul').fadeOut();
		$('#catbutton').click(function() {
			$('#catbutton').find('.glyphicon-chevron-up').addClass('rotate180');
			$('.catpanel').fadeIn("slow");
			$('.catpanel ul li').click(function(){
				$('.catpanel ul li').not(this).find('ul').fadeOut();
				$(this).find('ul').fadeIn();
			});
			
		});
		$('#catpanel').mouseleave(function() {
			$('.catpanel').fadeOut("slow");
			$('#catbutton').find('.glyphicon-menu-up').removeClass('rotate180');
		});
		$('.search-panel .dropdown-menu').find('a').click(function(e) {
			e.preventDefault();
			var param = $(this).attr("href").replace("#","");
			var concept = $(this).text();
			$('.search-panel span#search_concept').text(concept);
			$('.input-group #search_param').val(param);
		});
		var owl = $('.owl-carousel');
		owl.owlCarousel({
			items:4,
			loop:true,
			margin:10,
			nav:true,
			navText:['n','p'],
			autoplay:true,
			autoplayTimeout:1000,
			autoplayHoverPause:true,
			responsiveClass:true
			
		});

	});

	$.sidebarMenu = function(menu) {
		var animationSpeed = 300,
		subMenuSelector = '.sidebar-submenu';

		$(menu).on('click', 'li a', function(e) {
			var $this = $(this);
			var checkElement = $this.next();

			if (checkElement.is(subMenuSelector) && checkElement.is(':visible')) {
				checkElement.slideUp(animationSpeed, function() {
					checkElement.removeClass('menu-open');
				});
				checkElement.parent("li").removeClass("active");
			}
			else if ((checkElement.is(subMenuSelector)) && (!checkElement.is(':visible'))) {
				var parent = $this.parents('ul').first();
				var ul = parent.find('ul:visible').slideUp(animationSpeed);
				ul.removeClass('menu-open');
				var parent_li = $this.parent("li");
				checkElement.slideDown(animationSpeed, function() {
					checkElement.addClass('menu-open');
					parent.find('li.active').removeClass('active');
					parent_li.addClass('active');
				});
			}
			if (checkElement.is(subMenuSelector)) {
				e.preventDefault();
			}
		});
	}
  //imageshowing function
  function readURL(input) {
  	if (input.files && input.files[0]) {
  		var reader = new FileReader();

  		reader.onload = function (e) {
  			$('#imageShow')
  			.attr('src', e.target.result)
  			.width(150)
  			.height(100);
  			$('#imageShow')
  			.attr('style','display:block;')
  			.width(150)
  			.height(100);
  		};

  		reader.readAsDataURL(input.files[0]);
  	}
  }

