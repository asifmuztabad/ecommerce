<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', 'HomeController@index');
Route::get('/singleProductView/{id}', 'withoutauthservice@singleProductView');
Route::get('/',function () {
    return view('index');
});
Route::get('/V/{token}','Auth\RegisterController@verify');
Auth::routes();
//Users
Route::get('/home', 'HomeController@index');
Route::get('/logout', 'Auth\LoginController@logout');
//Admin
Route::get('/admin', 'AdminController@index');
Route::get('/addCategory', 'ProductController@category');
Route::get('/addSubCategory', 'ProductController@addSubCategory');
Route::post('/addCategory', 'ProductController@category');
Route::post('/addSubCategory', 'ProductController@addSubCategory');
Route::any('/product', 'ProductController@product');
Route::any('/getSubcatPro/{id}', 'ProductController@getSubcatPro');
