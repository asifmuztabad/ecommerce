<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
	protected $table='products';
	protected $fillable = [
	'product_category', 'product_sub_cat','product_title','product_manufacturer','product_quantity','product_model','product_genre','product_desc','product_buy_price','product_sale_price','product_status','product_viewed','product_image'
	];
	public static function getproductsbycategory($id)
	{
		$c=Products::where('product_category',$id)->get();
		return $c;
	}
	public static function getProductsForSinglePage($catid)
	{
		$c=Products::where('product_category',$catid)->orderBy('product_viewed','DESC')->get();
		return $c;
	}
}
