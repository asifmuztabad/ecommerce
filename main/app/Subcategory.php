<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
	protected $table='subcategory';
	protected $fillable = [
	'cat_id', 'subcat'
	];

	public static function getsubcategories($id) {
		$c=Subcategory::where('cat_id',$id)->get();
		return $c;
	}
}
