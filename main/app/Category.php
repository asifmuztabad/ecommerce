<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table='category';
	protected $fillable = [
	'cat_name', 'image'
	];

	public static function getcategories() {
    	$c=Category::get();
    	return $c;
	}
}
