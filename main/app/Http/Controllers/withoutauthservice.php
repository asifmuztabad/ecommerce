<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
class withoutauthservice extends Controller
{
	public function __construct()
	{
		
	}

	public function singleProductView($id)
	{	
		$id=$id;
		$getData=Products::where('id',$id)->get();
		$getData1=Products::where('id',$id)->first();
		$views=$getData1->product_viewed;
		Products::where('id',$id)->update(['product_viewed'=>$views+1]);
		if (!isset($id)) {
			return redirect('index');
		} else {
			return view('singleProductView')->with('getData',$getData);
		}
		
		
	}
}
