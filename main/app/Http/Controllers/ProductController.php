<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Subcategory;
use App\Products;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
class ProductController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}
	public function category(Request $request)
	{
		if ($request->isMethod('POST')) {
			$rules = [
			'cat_name' => 'required',
			'image' => 'required',
			];
			$validation = Validator::make($request->all(), $rules);
			$errors=$validation->errors();
			if ($validation->fails()) {
				Session::flash('flash_message', 'Please Check Again.');
				Session::flash('flash_type', 'alert-danger');
				return redirect()->back()->withErrors($validation->errors())->withinput($request->all());
			}
			else{
				$uploadDirectory = 'uploads/';
				$file = $request->file('image');
				$filename = $request->input('cat_name') . "." . $file->getClientOriginalName();
				$saveData = new Category();
				if ($file->move($uploadDirectory, $filename)) {
					$saveData->image = $filename;
					$saveData->cat_name = $request->input('cat_name');
					if($saveData->save()){
						Session::flash('flash_message', 'Successfully Created.');
						Session::flash('flash_type', 'alert-success');
						return redirect('addCategory');
					}
					else{
						Session::flash('flash_message', 'Problem Adding Category.');
						Session::flash('flash_type', 'alert-danger');
						return redirect('addCategory');
					}			
				}
				else{
					Session::flash('flash_message', 'Problem With Image.');
					Session::flash('flash_type', 'alert-danger');
					return redirect('addCategory');
				}
			}

		}
		else{
			return view('category');
		}
	}
	public function addSubCategory(Request $request)
	{
		if ($request->isMethod('POST')) {
			$rules = [
			'cat_id' => 'required',
			'subcat' => 'required',
			];
			$validation = Validator::make($request->all(), $rules);
			$errors=$validation->errors();
			if ($validation->fails()) {
				Session::flash('flash_message', 'Please Check Again.');
				Session::flash('flash_type', 'alert-danger');
				return redirect()->back()->withErrors($validation->errors())->withinput($request->all());
			}
			else{
				$saveData = new Subcategory();
				$saveData->cat_id = $request->input('cat_id');
				$saveData->subcat = $request->input('subcat');
				if($saveData->save()){
					Session::flash('flash_message', 'Successfully Added.');
					Session::flash('flash_type', 'alert-success');
					return redirect('addCategory');
				}
				else{
					Session::flash('flash_message', 'Problem With Adding.');
					Session::flash('flash_type', 'alert-danger');
					return redirect('addCategory');
				}

			}
		}
		else{
			return view('category');			
		}
	}
	public function product(Request $request)
	{
		if ($request->isMethod('POST')) {
			$rules = [
			'product_category' => 'required',
			'product_sub_cat' => 'required',
			'product_title' => 'required',
			'product_manufacturer' => 'required',
			'product_quantity' => 'required',
			'product_model' => 'required',
			'product_genre' => 'required',
			'product_desc' => 'required',
			'product_buy_price' => 'required',
			'product_sale_price' => 'required',
			'product_image' => 'required',
			];
			$validation = Validator::make($request->all(), $rules);
			$errors=$validation->errors();
			if ($validation->fails()) {
				Session::flash('flash_message', 'Please Check Again.');
				Session::flash('flash_type', 'alert-danger');
				return redirect()->back()->withErrors($validation->errors())->withinput($request->all());
			}
			else{
				$uploadDirectory = 'uploads/';
				$file = $request->file('product_image');
				$generatedid=Products::all()->last();
				if ($generatedid) {
					$g=$generatedid->id;
				} else {
					$g=0;
				}
				
				$filename =$g."." . $file->getClientOriginalName();
				if ($file->move($uploadDirectory, $filename)) {
					$saveData = new Products();
					$saveData->product_category = $request->input('product_category');
					$saveData->product_sub_cat = $request->input('product_sub_cat');
					$saveData->product_title = $request->input('product_title');
					$saveData->product_manufacturer = $request->input('product_manufacturer');
					$saveData->product_quantity = $request->input('product_quantity');
					$saveData->product_model = $request->input('product_model');
					$saveData->product_genre = $request->input('product_genre');
					$saveData->product_desc = $request->input('product_desc');
					$saveData->product_buy_price = $request->input('product_buy_price');
					$saveData->product_image = $filename;
					$saveData->product_status =1;
					$saveData->product_viewed =0;
					$saveData->product_sale_price = $request->input('product_sale_price');
					if($saveData->save()){
						Session::flash('flash_message', 'Successfully Added.');
						Session::flash('flash_type', 'alert-success');
						return redirect('product');
					}
					else{
						Session::flash('flash_message', 'Problem With Adding.');
						Session::flash('flash_type', 'alert-danger');
						return redirect('product');
					}

				}
				else{
					Session::flash('flash_message', 'Problem With Adding.');
					Session::flash('flash_type', 'alert-danger');
					return redirect('product');	
				}
			}
		}
		else{
			return view('product');
		}
	}
	public function getSubcatPro($id)
	{
		$data=Subcategory::where('cat_id',$id)->get();
		foreach ($data as $key) {
			echo "<option value='".$key->id."'>".$key->subcat."</option>";
		}
	}

}
