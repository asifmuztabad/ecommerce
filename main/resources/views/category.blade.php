@extends('DSmaster')

@section('content')
@include('includes.DSNavBar')
@include('includes.DSsideBar')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Category Add Panel</div>
			<div class="panel-body">
				<div class="col-md-6">
					<form action="{{url('/addCategory')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
						{!! csrf_field() !!}
						<div class="form-group{{ $errors->has('cat_name') ? ' has-error' : '' }}">
							<label for="cat_name" class="col-md-4 control-label">Category Name</label>

							<div class="col-md-6">
								<input id="cat_name" type="text" class="form-control" name="cat_name" value="{{ old('cat_name') }}" required autofocus>

								@if ($errors->has('cat_name'))
								<span class="help-block">
									<strong>{{ $errors->first('cat_name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
							<label for="image" class="col-md-4 control-label">Category Image</label>

							<div class="col-md-6">
								<input id="image" type="file" class="form-control" name="image" value="{{ old('image') }}" required onchange="readURL(this);">

								@if ($errors->has('image'))
								<span class="help-block">
									<strong>{{ $errors->first('image') }}</strong>
								</span>
								@endif
								<img id="imageShow" style="display: none;" src="" alt="your selected image will be shown here" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Add Category
								</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-6">
					<form action="{{url('/addSubCategory')}}" method="post" class="form-horizontal">
						{!! csrf_field() !!}
						<div class="form-group{{ $errors->has('cat_id') ? ' has-error' : '' }}">
							<label for="cat_id" class="col-md-4 control-label">Category Name</label>

							<div class="col-md-6">
								<select id="cat_id" class="form-control" name="cat_id" value="{{ old('cat_id') }}" required autofocus>
									@foreach(App\Category::getcategories() as $c)
									<option value="{{$c->id}}">{{$c->cat_name}}</option>
									@endforeach
								</select>
								@if ($errors->has('cat_id'))
								<span class="help-block">
									<strong>{{ $errors->first('cat_id') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('subcat') ? ' has-error' : '' }}">
							<label for="subcat" class="col-md-4 control-label">Sub Category Name</label>

							<div class="col-md-6">
								<input id="subcat" type="text" class="form-control" name="subcat" value="{{ old('subcat') }}" required autofocus>

								@if ($errors->has('subcat'))
								<span class="help-block">
									<strong>{{ $errors->first('subcat') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Add Sub Category
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Category List</div>
			<div class="panel-body">
				<table class="table table-bordered myTable">
					<thead>
						<tr>
							<th>SL No.</th>
							<th>Category Name</th>
							<th>Feature Image</th>
							<th>Subcategory</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<?php $i=1;?>
					@foreach(App\Category::getcategories() as $c)
						<tr>
							<td>{{$i}}</td>
							<td>{{$c->cat_name}}</td>
							<td><img src="uploads/{{$c->image}}" alt="Feature Image" class="img-rounded" width="50px" height="50px"></td>
							<td>@foreach(App\Subcategory::getsubcategories($c->id) as $ci)
									{{$ci->subcat}},
									@endforeach
							</td>
							<td>john@example.com</td>
						</tr>
						<?php $i++;?>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection