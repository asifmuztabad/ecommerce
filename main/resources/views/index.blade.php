@extends('MSmaster')

@section('content')
@include('includes.MSNavBar')
@include('includes.MSsearchBar')
@include('includes.slider')
@include('includes.MSsortBar')
@include('includes.MSProductPanel')
@endsection
