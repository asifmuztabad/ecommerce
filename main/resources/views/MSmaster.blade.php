@include('includes.MSheader')
<div class="flash-message">

      @if(Session::has('flash_message'))

      <p class="alert {{ Session::get('flash_type') }}">{{ Session::get('flash_message') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif

  </div> <!-- end .flash-message -->
@yield('content')
@include('includes.MSfooter')