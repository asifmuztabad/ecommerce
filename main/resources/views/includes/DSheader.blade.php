<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dashboard</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
	<link href="{{ asset('css/railwayfont.css') }}" rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/custom.css">
</head>
<body id="myscroll">
	<div class="siteLoader">
		<img src="{{ asset('img/squares.gif') }}" alt="">
	</div>	
	<div class="container-fluid mainSite">