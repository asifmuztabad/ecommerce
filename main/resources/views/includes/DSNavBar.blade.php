		<div class="row navbar navbar-green top-head">
			<div class="col-md-3">
				<div class="logo">
					<img class="navbar-brand" src="img/logo.png" alt="">
				</div>
			</div>
			<div class="col-md-5">
				<div class="input-group">
					<div class="input-group-btn search-panel">
						<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
							<span id="search_concept">Filter by</span> <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#contains">Contains</a></li>
							<li><a href="#its_equal">It's equal</a></li>
							<li><a href="#greather_than">Greather than ></a></li>
							<li><a href="#less_than">Less than < </a></li>
							<li class="divider"></li>
							<li><a href="#all">Anything</a></li>
						</ul>
					</div>
					<input type="hidden" name="search_param" value="all" id="search_param">         
					<input type="text" class="form-control" name="x" placeholder="Search term...">
					<span class="input-group-btn">
						<button class="btn btn-success" type="button"><span class="glyphicon glyphicon-search"></span></button>
					</span>
				</div>				
			</div>
			<div class="col-md-4">
				<div class="notHeader">
					<div class="col-xs-2">
						<span class="envalop"><i class="fa fa-envelope" aria-hidden="true"></i></span>
					</div>
					<div class="col-xs-2">
						<span class="envalop"><i class="fa fa-bell" aria-hidden="true"></i></span>
					</div>
					<div class="col-xs-2">
						<span class="envalop"><i class="fa fa-arrows-alt" aria-hidden="true"></i></span>
					</div>
					<div class="col-xs-6">
						<div class="userImage">
							<img src="img/user.png" alt="User Image">&nbsp;&nbsp;<span class="glyphicon glyphicon-chevron-down dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></span>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li class="dropdown-header">Nav header</li>
								<li><a href="#">Separated link</a></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>