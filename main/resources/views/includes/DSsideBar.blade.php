		<div class="row ">
			<div class="col-md-2 nomarginNopadding">
				<ul class="sidebar-menu">
					<li class="sidebar-header">DASHBOARD PANEL BAR</li>
					<li>
						<a href="#">
							<i class="fa fa-dashboard"></i> <span>Category Panel</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="sidebar-submenu">
							<li><a href="{{url('/addCategory')}}"><i class="fa fa-circle-o"></i> Add Category</a></li>
						</ul>
					</li>
					<li>
						<a href="#">
							<i class="fa fa-dashboard"></i> <span>Products</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="sidebar-submenu">
							<li><a href="{{url('/product')}}"><i class="fa fa-circle-o"></i> Add Product</a></li>
						</ul>
					</li>
					<li>
						<a href="#">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="sidebar-submenu">
							<li><a href=""><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
							<li><a href=""><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
						</ul>
					</li>
					<li>
						<a href="#">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="sidebar-submenu">
							<li><a href=""><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
							<li><a href=""><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
						</ul>
					</li>
				</ul>				
			</div>
			<div class="col-md-10 nomarginNopadding">
				<div class="dashMainContent">
					
