<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Online Shopping Portal</title>
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
	<link href="{{ asset('css/railwayfont.css') }}" rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.theme.default.css') }}">
	<link rel="stylesheet" href="{{ asset('css/magnify.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
</head>
<body id="myscroll">
			<div class="siteLoader">
				<img src="{{ asset('img/squares.gif') }}" alt="">
			</div>			
	<div class="container-fluid example2 mainSite">
