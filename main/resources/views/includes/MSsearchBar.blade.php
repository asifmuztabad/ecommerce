		<div class="row">
			<div class="col-md-4">
				<button class="btn btn-pcategories" id="catbutton"><span class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;Categories&nbsp;&nbsp;<span class="glyphicon glyphicon-menu-up"></span></button>
				<div class="catpanel" id="catpanel">
				@if(App\Category::getcategories())
					<ul class="catpanelUl">
						
						@foreach(App\Category::getcategories() as $c)
						<li class="catItem"><a href="#">{{$c->cat_name}}</a><span class="pull-right glyphicon glyphicon-menu-right"></span>
						@if(App\Subcategory::getsubcategories($c->id))
							<ul class="subcat">
							@foreach(App\Subcategory::getsubcategories($c->id) as $p)
								<li><a href="">{{$p->subcat}}</a></li>
							@endforeach
							</ul>
							@endif
						</li>
						@endforeach
					</ul>
					@endif
				</div>
			</div>
			<div class="col-md-5">	
				<div class="input-group">
					<div class="input-group-btn search-panel">
						<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
							<span id="search_concept">Filter by</span> <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#contains">Contains</a></li>
							<li><a href="#its_equal">It's equal</a></li>
							<li><a href="#greather_than">Greather than ></a></li>
							<li><a href="#less_than">Less than < </a></li>
							<li class="divider"></li>
							<li><a href="#all">Anything</a></li>
						</ul>
					</div>
					<input type="hidden" name="search_param" value="all" id="search_param">         
					<input type="text" class="form-control" name="x" placeholder="Search term...">
					<span class="input-group-btn">
						<button class="btn btn-success" type="button"><span class="glyphicon glyphicon-search"></span></button>
					</span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="userInfoBar">
					@if(Auth::guest())
					<div class="col-md-4">
						&nbsp;
					</div>
					@else
					<div class="col-md-4 userImage">
						<img src="{{ asset('img/user.png') }}" alt="User Image">&nbsp;&nbsp;<span class="glyphicon glyphicon-chevron-down dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></span>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li class="dropdown-header">Nav header</li>
							<li><a href="#">Separated link</a></li>
							<li><a href="#">One more separated link</a></li>
						</ul>
					</div>
					@endif
					<div class="col-md-4 cartMenu">
						<p><span class="glyphicon glyphicon-shopping-cart large"></span><span class="cartNumber">0</span></p>
					</div>
					@if(Auth::guest())
					<div class="col-md-3">
						<a href="{{url('/login')}}"><button class="btn btn-logIn">Log In</button></a>
					</div>
					@else
					<div class="col-md-3">
						<a href="{{url('/logout')}}"><button class="btn btn-logOut">Log Out</button></a>
					</div>
					@endif
				</div>
			</div>
		</div>