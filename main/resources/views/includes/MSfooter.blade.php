<div class="row footer">	
			<div class="col-md-3">	
				<div class="block-title"><strong>iconshop.com</strong></div>
				<div class="block-content">
					<ul class="links">
						<li><a href="">About iconshop</a></li>
						<li><a href="">Contact Us</a></li>
						<li><a href="">Our Customer Stories</a></li>
						<li></li>
						<li><b>MAKE MONEY WITH US</b></li>
						<li><a title="" href="">Become a Seller</a></li>
						<li><a title="" href="">Become an Affiliate Partner</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3">	
				<div class="block-title"><strong>iconshop.com</strong></div>
				<div class="block-content">
					<ul class="links">
						<li><a href="">About iconshop</a></li>
						<li><a href="">Contact Us</a></li>
						<li><a href="">Our Customer Stories</a></li>
						<li></li>
						<li><b>MAKE MONEY WITH US</b></li>
						<li><a title="" href="">Become a Seller</a></li>
						<li><a title="" href="">Become an Affiliate Partner</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3">	
				<div class="block-title">	<strong>We Accept</strong></div>
				<div class="block-content">	
					<div class="row">
						<div class="col-md-4">	<img class="weaccept-img" src="{{ asset('img/bkash-logo.jpg') }}" alt=""></div>
						<div class="col-md-4">	<img class="weaccept-img" src="{{ asset('img/cash-on-delivary-2.jpg') }}" alt=""></div>
						<div class="col-md-4">	<img class="weaccept-img" src="{{ asset('img/DBBL_logo.jpg') }}" alt=""></div>
					</div>
					<div class="row">
						<div class="col-md-4">	<img class="weaccept-img" src="{{ asset('img/MasterCard_Logo.jpg') }}" alt=""></div>
						<div class="col-md-4">	<img class="weaccept-img" src="{{ asset('img/swipe-on-delivery1.jpg') }}" alt=""></div>
						<div class="col-md-4">	<img class="weaccept-img" src="{{ asset('img/Visa_Inc._logo.jpg') }}" alt=""></div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<ul class="social">
					<li class="facebook"><a href="http://facebook.com/asifmuztaba14"><i class="fa fa-facebook fa-3x"></i></a></li>
					<li class="twitter"><a href="http://twitter.com/AsifMuztaba_"><i class="fa fa-twitter fa-3x"></i></a></li>
					<li class="pinterest"><a href="http://www.pinterest.com/"><i class="fa fa-pinterest-p fa-3x"></i></a></li>
					<li class="behance"><a href="https://www.behance.net/"><i class="fa fa-behance fa-3x"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 nomarginNopadding">
				<div class="dashFooter">
					<span>&copy 2017 || All Rights Reserved || Developed By: <a href=""> Md. Asif Muztaba</a></span>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>


<script src="{{ asset('js/owl.carousel.js') }}"></script>
<script src="{{ asset('js/jquery.magnify.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
</html>