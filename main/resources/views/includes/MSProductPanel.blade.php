@foreach(App\Category::getcategories() as $c)
@if(App\Products::getproductsbycategory($c->id))
<div class="row">
	<div class="col-md-9">
		<div class="owl-carousel owl-theme">

		@foreach(App\Products::getproductsbycategory($c->id) as $p)
			<div class="item">
				<img src="{{ asset('uploads')}}/{{$p->product_image}}" alt="">
				<div class="plebel">
					<h4><a href="{{url('singleProductView')}}/{{$p->id}}"><strong></strong>{{$p->product_title}} </a></h4>
					<h5><strong>Price:</strong>{{$p->product_sale_price}}/- BDT</h5>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	<div class="col-md-3">
		<div class="seeMore">
			<img src="{{ asset('uploads') }}/{{$c->image}}" alt="">
			<button class="btn btn-seemore">See More</button>
		</div>
	</div>
</div>
@else
@endif
<br>
@endforeach
