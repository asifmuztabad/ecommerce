@extends('MSmaster')

@section('content')
@include('includes.MSNavBar')
@include('includes.MSsearchBar')
<div class="row">
	<div class="col-md-8">
		<div class="singlproductView">
		@foreach($getData as $g)
		<?php $idf=$g->product_category; ?>
			<h3 class="page-header heading-shade">{{$g->product_title}}</h3>
			<div class="col-md-7">
				<img src="{{ asset('uploads') }}/{{$g->product_image}}" class="img-responsive zoom" data-magnify-src="{{ asset('uploads') }}/{{$g->product_image}}" alt="" width="400" height="400">
			</div>
			<div class="col-md-5">
				<h3>{{$g->product_title}}</h3>
				<hr>
				<p>{{$g->product_desc}}</p>
				<hr>
				<p><strong>Price:</strong>{{$g->product_sale_price}} <strong>BDT</strong>
				<a href=""><button class="pull-right btn btn-success">Add To Cart</button></a>
				</p>

			</div>
			@endforeach
		</div>
	</div>
	<div class="col-md-4">
		<h3 class="page-header heading-shade" style="margin-bottom: 0">Similiar Items</h3>
@if(App\Products::getProductsForSinglePage($idf))
@foreach(App\Products::getProductsForSinglePage($idf) as $c)
		<div class="singlePageItemContainer">
			<div class="col-md-4">
				<img src="{{ asset('uploads') }}/{{$c->product_image}}" alt="" class="img-rounded img-responsive" width="100" height="100">
			</div>
			<div class="col-md-8">
				<h4 class="page-header">{{$c->product_title}}</h4>
				<p>{{$c->product_desc}}</p>
			</div>
		</div>
		@endforeach
		@endif
	</div>
</div>
@endsection