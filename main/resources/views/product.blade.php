@extends('DSmaster')

@section('content')
@include('includes.DSNavBar')
@include('includes.DSsideBar')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Add Product</div>
			<div class="panel-body">
				<form action="{{url('/product')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="col-md-6">
						{!! csrf_field() !!}
						<div class="form-group{{ $errors->has('proAddCat') ? ' has-error' : '' }}">
							<label for="proAddCat" class="col-md-4 control-label">Category Name</label>

							<div class="col-md-6">
								<select id="proAddCat" class="form-control" name="product_category" value="{{ old('proAddCat') }}" required autofocus>
								<option value="">Select a category</option>
									@foreach(App\Category::getcategories() as $c)
									<option value="{{$c->id}}">{{$c->cat_name}}</option>
									@endforeach
								</select>
								@if ($errors->has('proAddCat'))
								<span class="help-block">
									<strong>{{ $errors->first('proAddCat') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('product_title') ? ' has-error' : '' }}">
							<label for="product_title" class="col-md-4 control-label">Product Title</label>

							<div class="col-md-6">
								<input id="product_title" type="text" class="form-control" name="product_title" value="{{ old('product_title') }}" required autofocus>

								@if ($errors->has('product_title'))
								<span class="help-block">
									<strong>{{ $errors->first('product_title') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('product_manufacturer') ? ' has-error' : '' }}">
							<label for="product_manufacturer" class="col-md-4 control-label">Product Manufacturer</label>

							<div class="col-md-6">
								<input id="product_manufacturer" type="text" class="form-control" name="product_manufacturer" value="{{ old('product_manufacturer') }}" required autofocus>
								@if ($errors->has('product_manufacturer'))
								<span class="help-block">
									<strong>{{ $errors->first('product_manufacturer') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('product_quantity') ? ' has-error' : '' }}">
							<label for="product_quantity" class="col-md-4 control-label">Product Quantity</label>

							<div class="col-md-6">
								<input id="product_quantity" type="number" class="form-control" name="product_quantity" value="{{ old('product_quantity') }}" required autofocus>
								@if ($errors->has('product_quantity'))
								<span class="help-block">
									<strong>{{ $errors->first('product_quantity') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('product_model') ? ' has-error' : '' }}">
							<label for="product_model" class="col-md-4 control-label">Product Model</label>

							<div class="col-md-6">
								<input id="product_model" type="text" class="form-control" name="product_model" value="{{ old('product_model') }}" required autofocus>

								@if ($errors->has('product_model'))
								<span class="help-block">
									<strong>{{ $errors->first('product_model') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('product_genre') ? ' has-error' : '' }}">
							<label for="product_genre" class="col-md-4 control-label">Product Genre</label>

							<div class="col-md-6">
								<select id="product_genre" class="form-control" name="product_genre" value="{{ old('product_genre') }}" required autofocus>
									
									<option value="M">Male</option>
									<option value="F">Female</option>
									<option value="B">Both</option>
									<option value="O">Materials</option>

								</select>
								@if ($errors->has('product_genre'))
								<span class="help-block">
									<strong>{{ $errors->first('product_genre') }}</strong>
								</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{{ $errors->has('proAddsubcat') ? ' has-error' : '' }}">
							<label for="proAddsubcat" class="col-md-4 control-label">Sub Category Name</label>

							<div class="col-md-6">
								<input type="hidden" name="proAddsubcatToken" id="proAddsubcatToken" value="{{csrf_token()}}">
								<select id="proAddsubcat" class="form-control" name="product_sub_cat" value="{{ old('proAddsubcat') }}" required>

								</select>
								@if ($errors->has('proAddsubcat'))
								<span class="help-block">
									<strong>{{ $errors->first('proAddsubcat') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('product_image') ? ' has-error' : '' }}">
							<label for="product_image" class="col-md-4 control-label">Product Image</label>

							<div class="col-md-6">
								<div class="col-md-9">
									<input id="product_image" type="file" class="form-control" name="product_image" value="{{ old('product_image') }}" required onchange="readURL(this);">
									
									@if ($errors->has('product_image'))
									<span class="help-block">
										<strong>{{ $errors->first('product_image') }}</strong>
									</span>
									@endif
								</div>
								<div class="col-md-3">
									<img id="imageShow" style="display: none;" src="" alt="your selected image will be shown here" />
								</div>
							</div>
						</div>
						<div class="form-group{{ $errors->has('product_desc') ? ' has-error' : '' }}">
							<label for="product_desc" class="col-md-4 control-label">Product Description</label>

							<div class="col-md-6">
								<textarea id="product_desc" type="text" class="form-control" name="product_desc" value="{{ old('product_desc') }}" required></textarea>

								@if ($errors->has('product_desc'))
								<span class="help-block">
									<strong>{{ $errors->first('product_desc') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('product_buy_price') ? ' has-error' : '' }}">
							<label for="product_buy_price" class="col-md-4 control-label">Product Buy Price</label>

							<div class="col-md-6">
								<input id="product_buy_price" type="number" class="form-control" name="product_buy_price" value="{{ old('product_buy_price') }}" required autofocus>
								@if ($errors->has('product_buy_price'))
								<span class="help-block">
									<strong>{{ $errors->first('product_buy_price') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('product_sale_price') ? ' has-error' : '' }}">
							<label for="product_sale_price" class="col-md-4 control-label">Product Sell Price</label>

							<div class="col-md-6">
								<input id="product_sale_price" type="number" class="form-control" name="product_sale_price" value="{{ old('product_sale_price') }}" required autofocus>
								@if ($errors->has('product_sale_price'))
								<span class="help-block">
									<strong>{{ $errors->first('product_sale_price') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Add Product
								</button>&nbsp;&nbsp;
								<button type="reset" class="btn btn-warning">
									Reset Fileds
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection