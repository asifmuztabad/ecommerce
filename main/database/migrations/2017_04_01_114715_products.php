<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_title');
            $table->string('product_desc');
            $table->string('product_model');
            $table->string('product_image');
            $table->string('product_category');
            $table->string('product_sub_cat');
            $table->string('product_weight');
            $table->string('product_buy_price');
            $table->string('product_sale_price');
            $table->string('product_status');
            $table->string('product_quantity');
            $table->string('product_date_added');
            $table->string('product_manufacturer');
            $table->string('product_viewed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
